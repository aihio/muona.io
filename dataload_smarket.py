# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 17:03:42 2015

@author: jukka
"""

#http://www.kauppalehti.fi/5/i/porssi/porssikurssit/kurssihistoria.jsp

from pyvirtualdisplay import Display
#from xvfbwrapper import Xvfb
import selenium

import time
import sys
import selenium
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from lxml import html
import datetime

root = ' /Users/jukkasaarelma/Documents/koodit/m_food/scraped_data/smarket'

html_root = "https://www.foodie.fi/products/selection"

fp = webdriver.FirefoxProfile()

fp.set_preference("browser.download.folderList",2)
fp.set_preference("browser.download.manager.showWhenStarting",False)
fp.set_preference("browser.download.dir", root)
fp.set_preference("browser.helperApps.neverAsk.saveToDisk","application/vnd.ms-excel")
fp.set_preference("browser.download.manager.alertOnEXEOpen", False)

display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Firefox(firefox_profile=fp)
driver.get(html_root)

#action = selenium.webdriver.ActionChains(driver)

#pv = driver.find_element_by_name("kpv")


def tryKeys(d_key):
  global pv
  try:
    time.sleep(0.1)
    pv.send_keys(d_key)
  except selenium.common.exceptions.ElementNotVisibleException:
    print "Element not visible, wait, and try again"
    time.sleep(2)
    tryKeys(d_key)

page = driver.execute_script("return document.documentElement.outerHTML;")
tree = html.fromstring(page)

main_div = tree.xpath('//div[@class="categories-shelf clearfix"]')
raw_links = main_div[0].xpath('//li')
raw_links = set(raw_links)
raw_links = list(raw_links)


links=[]
for i in range(len(raw_links)):
  cur_ref = raw_links[i].xpath('.//a/@href')
  for c in cur_ref:
    if "roduct" in c:
      links.append(c)

#%%
import json


for j in range(50, len(links)):
  driver.get("https://www.foodie.fi%s"%links[j])
  prod_page = driver.execute_script("return document.documentElement.outerHTML;")
  prod_tree = html.fromstring(prod_page)
  prod_divs = prod_tree.xpath('//div[@class="info relative clear"]')
  prod_ul = prod_tree.xpath('//ul[@class="shelf js-shelf clear clearfix"]')
  if len(prod_ul)==0:
    continue
  prod_links = prod_ul[0].xpath('.//a[@class="js-link-item"]/@href')
  data = []


  for i in range(len(prod_links)):
    driver.get("https://www.foodie.fi%s"%prod_links[i])
    try:
      pv = driver.find_element_by_link_text("Ravintosisältö")
    except:
      break
    pv.click()

    #%
    def getProdData(page_):
      name_=page_.xpath('//h1[@id="product-name"]/text()')
      price = float(page_.xpath('//span[@class="whole-number "]/text()')[0])
      price_dec = float(page_.xpath('//span[@class="decimal"]/text()')[0])
      weight = page_.xpath('//div[@class="js-quantity"]/text()')[0]
      vals = []
      price+=0.01*price_dec

      vals.append(["Nimi", name_])
      vals.append(["Hinta", price])
      vals.append(["Määrä", weight])
      rows = page_.xpath('//tr')

      for r in rows:
        name = r.xpath('.//td[@class="name"]/text()')
        if len(name)==0:
          name = r.xpath('.//td[@class="name "]/text()')
        if len(name)==0:
          name = r.xpath('.//td[@class="name sub"]/text()')
        val = r.xpath('.//td[@class="text-right"]/text()')

        if len(name)!=0 and len(val)!=0:
          vals.append([name[0], val[0]])

      return vals


    page_ = html.fromstring(driver.execute_script("return document.documentElement.outerHTML;"))

    name_=page_.xpath('//h1[@id="product-name"]/text()')
    price = page_.xpath('//span[@class="whole-number "]/text()')
    price_dec = page_.xpath('//span[@class="decimal"]/text()')
    weight = page_.xpath('//div[@class="js-quantity"]/text()')

    if len(price)==0:
      break

    vals = []
    price = float(price[0])+0.01*float(price_dec[0])

    vals.append(["Nimi", name_])
    vals.append(["Hinta", price])
    vals.append(["Määrä", weight[0]])
    rows = page_.xpath('//tr')

    for r in rows:
      name = r.xpath('.//td[@class="name"]/text()')
      if len(name)==0:
        name = r.xpath('.//td[@class="name "]/text()')
      if len(name)==0:
        name = r.xpath('.//td[@class="name sub"]/text()')
      val = r.xpath('.//td[@class="text-right"]/text()')

      if len(name)!=0 and len(val)!=0:
        vals.append([name[0], val[0]])

    #v = getProdData(cur_tree)
    data.append(vals)


  data_dict = {}

  for d in data:
    cur_dict = {}
    for k in d[1:]:
      cur_dict[k[0]]=k[1]

    data_dict[d[0][1][0]] = cur_dict


  f = open("data_%u.json"%j, "w")
  json.dump(data_dict, f)
  f.close()





