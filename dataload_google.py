# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 17:03:42 2015

@author: jukka
"""

#http://www.kauppalehti.fi/5/i/porssi/porssikurssit/kurssihistoria.jsp

from pyvirtualdisplay import Display
#from xvfbwrapper import Xvfb
import selenium

import time
import sys
import selenium
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import datetime
from lxml import html
import json

root = '/Users/jukkasaarelma/Documents/koodit/m_food/scraped_data/google'

html_root = "https://www.google.fi"

fp = webdriver.FirefoxProfile()

fp.set_preference("browser.download.folderList",2)
fp.set_preference("browser.download.manager.showWhenStarting",False)
fp.set_preference("browser.download.dir", root)
fp.set_preference("browser.helperApps.neverAsk.saveToDisk","application/vnd.ms-excel")
fp.set_preference("browser.download.manager.alertOnEXEOpen", False)

display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Firefox(firefox_profile=fp)
driver.get(html_root)

# Fetch google search field
pv = driver.find_element_by_name("q")

def tryKeys(d_key):
  global pv
  try:
    time.sleep(0.1)
    pv.send_keys(d_key)
  except selenium.common.exceptions.ElementNotVisibleException:
    print "Element not visible, wait, and try again"
    time.sleep(2)
    tryKeys(d_key)


tryKeys("kalapuikko")

# Fetch google search button
pv = driver.find_element_by_name("btnK")
pv.click()
right_div="kp-blk knowledge-panel _Rqb _RJe"

page = driver.execute_script("return document.documentElement.outerHTML;")
tree = html.fromstring(page)

#pane = tree.xpath('//div[@class="kp-blk knowledge-panel _Rqb _RJe"]')
#maara = tree.xpath('//*[text()="Määrä"]')
title = tree.xpath('//span[@class="_VLb"]/text()')
maara = tree.xpath('//tr[@class="kno-nf-sbl"]/span/text()')
maara = tree.xpath('//tr[@class="kno-nf-nr"]')

data = []
for i in range(len(maara)):
  print i
  data.append(maara[i].xpath(".//span/text()"))

rows = tree.xpath('//tr')
raw_titles = []
raw_vals = []

for i in range(len(rows)):
  cur_title=rows[i].xpath('.//td[@class="ellip"]/text()')
  for c in cur_title:
    if(len(c)>2):
      raw_titles.append(c)
  cur_val = rows[i].xpath('.//span[@class="pdv"]/text()')
  for c in cur_val:
    if(len(c)>1):
      raw_vals.append(c)

for i in range(len(raw_vals)):
  data.append([raw_titles[i], raw_vals[i]])


#json.
data_dict = {}
for i in range(len(data)):
  cur_data= data[i]
  if len(cur_data)>1:
    data_dict[data[i][0]]=data[i][1]

print json.dumps(data_dict)
#ravintola_nimet = tree.xpath('//div[@class="bicc"]')

#print "Try keys + xpath"
#
#tryKeys(d_key)
#
#pv = driver.find_element_by_xpath("//*[@value='excel']")
#pv.click()
#elem = driver.find_element_by_xpath("//*[@value='Hae kurssit']")
#elem.click()
#
#print "Into file fetch"
#start = time.time()
#
#def fetchFile(fname):
#  while not os.path.isfile(fname):
#    print "Waiting for: "+ fname
#    time.sleep(0.1)
#    elaps = time.time()-start
#    if elaps > 10:
#      print "20 seconds gone waiting for " + fname + ", continuing"
#      continue
#
#  print "Fetched " +str(d_key) + " time elapsed " + str(time.time()-start)
#
#  stats = os.stat(fname)
#  if(stats.st_size == 0):
#    print fname + " File fetched, but size is 0, removing and tryting again"
#    time.sleep(0.1)
#    fetchFile(fname)
#
#
#fetchFile(fname)
#
#driver.quit()
#display.stop()
#del driver
#del display
#del fp
##  vdisplay.stop()
#cur += datetime.timedelta(days=1)
#
#



#elem.send_keys( '15' )
#elem.send_keys( Keys.RETURN )
